import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import bookmarks from './reducers';
import BookmarkApp from './components/App';
import SearchAside from './components/SearchAside';
import 'react-select/dist/react-select.css';
import 'material-components-web/dist/material-components-web.css';
import './styles/index.css';
import './assets/uiEvents';

const store = createStore(bookmarks);

render(
    <Provider store={store}>
        <BookmarkApp />
    </Provider>,
  document.getElementById('root')
);

render(
    <Provider store={store}>
        <SearchAside />
    </Provider>,
    document.getElementById('search-aside')
);
