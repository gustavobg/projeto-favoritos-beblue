import React from 'react';
import reducer from '../reducers/tags';

describe('reducer tags', () => {
    it('Deve-se adicionar uma nova tag ao final do índice', () => {
        expect(
            reducer([{ label: 'Decoração', name: 'Decoração', color: '#F44336' }], {
                type: 'ADD_TAG',
                id: 'Tag!',
                color: '#cccccc'
            })
        ).toEqual([{"color": "#F44336", "label": "Decoração", "name": "Decoração"}, {"color": "#cccccc", "id": "Tag!", "name": "Tag!"}])
    });
});