import React from 'react';
import ReactDOM from 'react-dom';
import reducer from '../reducers/bookmarks';

it('react ok', () => {
    const div = document.createElement('div');
    ReactDOM.render(<div />, div)
});
describe('reducer bookmarks', () => {
    it('Deve-se criar um novo favorito no início do índice', () => {
        expect(
            reducer([{id: 1, url: 'http://www.teste.com', tags: ['Interessante!', 'Uau!']}], {
                type: 'ADD_BOOKMARK',
                id: '2',
                url: 'http://',
                title: 'Teste!',
                tags: ['Olá!']
            })
        ).toEqual([{"id": "2", "tags": ['Olá!'], "title": "Teste!", "url": "http://"}, {"id": 1, "tags": ["Interessante!", "Uau!"], "url": "http://www.teste.com"}])
    });
    it('Deve-se alterar o favorito', () => {
        expect(
            reducer([{id: 1, url: 'http://www.teste.com', tags: ['Interessante!', 'Uau!']}], {
                type: 'EDIT_BOOKMARK',
                id: 1,
                url: 'http://',
                title: 'Teste!',
                tags: ['Olá!']
            })
        ).toEqual([{"id": 1, "tags": ["Olá!"], "title": "Teste!", "url": "http://"}])
    });
    it('Deve-se remover o favorito', () => {
        expect(
            reducer([{id: 1, url: 'http://www.teste.com', tags: ['Interessante!', 'Uau!']}], {
                type: 'REMOVE_BOOKMARK',
                id: 1
            })
        ).toEqual([])
    });
});