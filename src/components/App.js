import React from 'react';
import BookmarkAdd from './BookmarkAdd';
import BookmarkList from './BookmarkList';


class App extends React.Component {
    componentDidMount() {
        document.getElementById('txtUrlAdd').focus();
        //window.setTimeout(() => document.body.classList.add('show-app'), 500);
        document.body.classList.add('show-app');
    }
    render() {
        return (
            <div>
                <BookmarkAdd />
                <BookmarkList />
            </div>
        )
    }
}

export default App;
