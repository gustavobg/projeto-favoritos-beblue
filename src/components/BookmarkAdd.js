import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addBookmark, addTag } from '../actions';
import SelectCreate from './SelectCreate';


// that tells how to transform the current Redux store state into the props
// you want to pass to a presentational component you are wrapping
const mapStateToProps = (state) => {
    return {
        allTags: state.tags,
    };
};

// In addition to reading the state, container components can dispatch actions.
// In a similar fashion, you can define a function called mapDispatchToProps() that receives the dispatch() method
// and returns callback props that you want to inject into the presentational component
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onSubmit: (name, url, tags, allTags) => {
            let bookmarkState = dispatch(addBookmark(name, url, tags));
            if (tags.length > 0) {
                let allTagsArr = allTags.map((t) => t.name);
                bookmarkState.tags.map((x) => {
                    if (allTagsArr.indexOf(x) === -1) {
                        dispatch(addTag(x));
                    }
                });
            }
        }
    };
};

class BookmarkAdd extends Component {

    constructor() {
        super();
        this.state = {
            tags: ''
        }
    }

    save(e) {
        e.preventDefault();
        let tagsToState = this.state.tags.length > 0 ? this.state.tags.map(t => t.label) : [];
        this.props.onSubmit(this.titleInput.value, this.urlInput.value, tagsToState, this.props.allTags);
        this.clearInputs();
    }

    clearInputs() {
        this.titleInput.value = '';
        this.urlInput.value = '';
        this.setState({ tags: '' });
    }

    handleOnChange(value) {
        this.setState({ tags: value });
    }

    render() {
        let tagsFromState = this.props.allTags.map((tag) => ({
            label: tag.name,
            value: tag.name
        }));

        return (
            <form className="form-add-tag" onSubmit={this.save.bind(this)}>
                <div className="container-form">
                    <div className="form-group">
                        <div className="mdc-textfield" data-mdc-auto-init="MDCTextfield">
                            <input id="txtUrlAdd" placeholder="URL" className="mdc-textfield__input" ref={node => { this.urlInput = node }} type="text" />
                            <label htmlFor="txtUrlAdd" className="mdc-textfield__label"></label>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="mdc-textfield" data-mdc-auto-init="MDCTextfield">
                            <input id="txtTituloAdd" placeholder="Título" className="mdc-textfield__input" ref={node => { this.titleInput = node; }} type="text"/>
                            <label htmlFor="txtTituloAdd" className="mdc-textfield__label"></label>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="tags-add">
                            <SelectCreate options={tagsFromState} value={this.state.tags} onChange={this.handleOnChange.bind(this)} />
                        </div>
                    </div>
                </div>
                <div className="form-group button-add-container">
                    <button type="submit" className="mdc-button mdc-button--raised mdc-button--primary">
                        Adicionar favorito
                    </button>
                </div>
            </form>
        )
    }
}

BookmarkAdd.propTypes = {
    url: PropTypes.string.isRequired,
    name: PropTypes.string,
    tags: PropTypes.array
};
BookmarkAdd.defaultProps = {
    url: '',
    name: '',
    tags: []
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BookmarkAdd);
