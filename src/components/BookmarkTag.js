import React from 'react';
import { connect } from 'react-redux';
import { filterByTag } from '../actions/index';

const mapDispatchToProps = (dispatch) => {
    return {
        filterByTag: (tagName) => {
            dispatch(filterByTag(tagName))
        }
    }
};

class Tag extends React.Component {

    tagFilter(e) {
        e.preventDefault();
        let element = e.target;
        let tagNameFilter = this.props.tag.name;
        let isSelected = element.classList.contains('selected');

        if (isSelected) {
            tagNameFilter = '';
        }

        // remove todos selecionados
        document.querySelectorAll('.tag').forEach((element) =>
            element.classList.remove('selected')
        );

        if (!isSelected) {
            element.classList.add('selected');
        }

        this.props.filterByTag(tagNameFilter);
    }

    render() {
        const style = {
          backgroundColor: this.props.tag.color
        };
        return (
            <button className="tag" style={style} onClick={this.tagFilter.bind(this)}
                    aria-pressed="false"
                    aria-label={"Filtrar por tag: " + this.props.tag.name}
                    title={"Filtrar por tag: " + this.props.tag.name}>
                {this.props.tag.name}
            </button>
        )
    }
}
export default connect(null, mapDispatchToProps)(Tag);