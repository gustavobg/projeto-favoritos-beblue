import React from 'react';
import Select from 'react-select';
import { connect } from 'react-redux';
import _ from 'lodash';

const mapStateToProps = (store) => {
    return {
        allTags: store.tags
    };
};

const SelectCreate = ({ options, value, onChange, allTags }) => {
    let getTagColor = (name) => {
        let arr = _.filter(
            allTags,
            tag => {
                return tag.name === name;
            });
        if (arr.length > 0) {
            return arr[0].color;
        } else {
            return '#009688';
        }
    };
    return <Select.Creatable
        name="select-tags"
        multi
        shouldKeyDownEventCreateNewOption={(params) => { return [32, 13, 9].indexOf(params.keyCode) > -1}} // não permite espaços e adiciona tag
        promptTextCreator={(label) => 'Criar nova tag: "' + label + '"'}
        value={value}
        options={options}
        onChange={onChange}
        clearable={false}
        searchPromptText="Digite para buscar a tag"
        noResultsText="Nenhuma tag encontrada"
        clearValueText="Limpar tag"
        clearAllText="Limpar todos"
        placeholder="Selecionar ou criar nova tag"
        valueRenderer={(tag) => {
            return <button style={{backgroundColor: getTagColor(tag.label) }} className="tag">{tag.label}</button>
        }}
    />
};

export default connect(mapStateToProps)(SelectCreate);