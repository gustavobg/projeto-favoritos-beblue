import React from 'react';
import PropTypes from 'prop-types';
import SelectCreate from './SelectCreate';

import { editBookmark, removeBookmark, addTag } from '../actions';
import { connect } from 'react-redux';


const mapStateToProps = (store) => {
    return {
        allTags: store.tags
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        editBookmark: (id, title, url, tags, allTags) => {
            let bookmarkState = dispatch(editBookmark(id, title, url, tags));
            let allTagsArr = allTags.map((t) => t.name);
            bookmarkState.tags.map((x) => {
                if (allTagsArr.indexOf(x) === -1) {
                    dispatch(addTag(x));
                }
            });
        },
        removeBookmark: (id) => {
            dispatch(removeBookmark(id))
        }
    };
};

class BookmarkRow extends React.Component {
    constructor() {
        super();
        this.state = {
            title: '',
            url: '',
            tags: ''
        }
    }

    removeBookmark () {
        this.props.removeBookmark(this.props.bookmark.id);
    }


    componentDidMount() {
        let selectedTagsToInitialValue = this.props.bookmark.tags.map((tag) => ({
            label: tag,
            value: tag
        }));
        this.setState({
            tags: selectedTagsToInitialValue,
            url: this.props.bookmark.url,
            title: this.props.bookmark.title
        });
    }

    updateStore() {
        let tagsToStore = this.state.tags.map((tag) => {
            return tag.label;
        });
        this.props.editBookmark(this.props.bookmark.id, this.state.title, this.state.url, tagsToStore, this.props.allTags);
    }

    handleOnChangeSelect(value) {
        this.setState({
            tags: value
        }, () => this.updateStore());
    }

    handleOnChangeEditable (event) {
        const target = event.target;
        const value = target.innerText;
        const name = target.getAttribute('name');

        this.setState({
            [name]: value
        }, () => this.updateStore());
    };

    render() {
        let tagsFromStateToOptions = this.props.allTags.map((tag) => ({
            label: tag.name,
            value: tag.name,
            color: tag.color
        }));
        return (
            <div className="bookmark-item">
                <button className="remove-bookmark material-icons" onClick={this.removeBookmark.bind(this)} role="button" aria-pressed="false" aria-label="Remover favorito">
                    clear
                </button>
                <div className="flex-start">
                    <p className="title editable" contentEditable={true} name="title" onBlur={this.handleOnChangeEditable.bind(this)}>{this.state.title}</p>
                    <p className="url editable" contentEditable={true} name="url" onBlur={this.handleOnChangeEditable.bind(this)}>{this.state.url}</p>
                </div>
                <div className="tag-list-container">
                    <SelectCreate options={tagsFromStateToOptions} value={this.state.tags} onChange={this.handleOnChangeSelect.bind(this)} />
                </div>
            </div>
        )
    }
}

BookmarkRow.PropTypes = {
    id: PropTypes.number,
    title: PropTypes.string,
    url: PropTypes.string.isRequired,
    tags: PropTypes.array
};

export default connect(mapStateToProps, mapDispatchToProps)(BookmarkRow);
