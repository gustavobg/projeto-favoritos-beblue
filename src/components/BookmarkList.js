import React from 'react';
import { connect } from 'react-redux';
import BookmarkRow from './BookmarkRow';
import bookmarksFiltered from '../selectors/bookmarks';

const mapStateToProps = (state) => {
    return {
        bookmarksFiltered: bookmarksFiltered(state)
    };
};

const BookmarkList = ({ bookmarksFiltered }) => {
    return (
        <ul className="bookmarks-list">
            {bookmarksFiltered.map(bookmark =>
                <li key={bookmark.id}>
                    <BookmarkRow bookmark={bookmark} />
                </li>
            )}
        </ul>
    )
};


export default connect(
    mapStateToProps
)(BookmarkList);