import React from 'react';
import { connect } from 'react-redux';
import Tag from './BookmarkTag';
import { filterByText } from '../actions/index';

const mapStateToProps = (state) => {
    return {
        allTags: state.tags,
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        filterByText: (text) => {
            dispatch(filterByText(text))
        }
    }
};

class SearchAside extends React.Component {
    textFilter(e) {
        this.props.filterByText(e.target.value);
    }
    render() {
        return (
            <div>
                <header>
                    <i className="material-icons">search</i>
                    <input id="search-input" onKeyUp={this.textFilter.bind(this)} type="search" placeholder="Buscar favorito"/>
                </header>
                <div className="content" id="tags-search">
                    <h2 className="content-label">Filtro por tags</h2>
                    {this.props.allTags.map((tag) =>
                        <Tag tag={tag} key={tag.name}/>
                    )}
                </div>
            </div>)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchAside);


