import { combineReducers } from 'redux';
import bookmarks from './bookmarks';
import tags from './tags';
import filters from './filters';

const bookmarksApp = combineReducers({
    bookmarks,
    tags,
    filters
});

export default bookmarksApp;
