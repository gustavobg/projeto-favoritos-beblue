// set initial state
let initialState = [
    { label: 'Beblüe', name: 'Beblüe', color: '#1f68c1' },
    { label: 'Interessante', name: 'Interessante', color: '#795548' },
    { label: 'Front-end', name: 'Front-end', color: '#FFC107' },
    { label: 'Importante!', name: 'Importante!', color: '#009688' },
    { label: 'Decoração', name: 'Decoração', color: '#F44336' }
];

const tags = (state = initialState, action) => {
    /*
     GET_BOOKMARK_BY_ID
     GET_BOOKMARKS_BY_TAGS_ID
     */
    switch (action.type) {
        case 'ADD_TAG':
            return [
                ...state, // concat :)
                {
                    id: action.id,
                    name: action.id,
                    color: action.color
                }
            ];
        case 'GET_TAG_BY_ID':
            // todo
            return {
                ...state,
                ...state.reduce((obj, tag) => {
                    obj[tag.name] = tag;
                    return obj
                }, {})
            };
        default:
            return state;
    }
};

export default tags;