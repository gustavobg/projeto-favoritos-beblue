import Chance from 'chance';

let initialState = [];
let i = 0;
let t = 0;
let arrTags = ['Beblüe', 'Interessante', 'Front-end', 'Importante!', 'Decoração'];
let chance = new Chance();
for (i; i < 10;) {
    initialState.push({ id: i, title: chance.name(), url: chance.url(), tags: [arrTags[t]] });
    t++;
    i++;
    t = t === 5 ? 0 : t;
}

const bookmarks = (state = initialState, action) => {

    switch (action.type) {
        case 'ADD_BOOKMARK': {
            // check if user added new tags
            return [
                {
                    id: action.id,
                    url: action.url,
                    title: action.title,
                    tags: action.tags,
                }, ...state // concat :)
            ]
        }
        case 'EDIT_BOOKMARK':
            return state.map(bookmark => {
                if (bookmark.id === action.id) {
                    return { ...bookmark, title: action.title, url: action.url, tags: action.tags }
                } else {
                    return bookmark
                }
            });
        case 'REMOVE_BOOKMARK':
            return state.filter(bookmark => bookmark.id !== action.id);
        default:
            return state;
    }
};

export default bookmarks
