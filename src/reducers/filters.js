const initialState = {
    searchText: '',
    tagFilter: ''
};

const filters = (state = initialState, action) => {
    switch (action.type) {
        case 'FILTER_BY_TAG':
            return Object.assign({}, state, { tagFilter: action.tagName });
        case 'FILTER_BY_TEXT':
            return Object.assign({}, state, { searchText: action.text });
        default:
            return state;
    }
};

export default filters;
