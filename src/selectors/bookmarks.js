import { createSelector } from 'reselect';
import _ from 'lodash';

const bookmarksSelector = (state) => state.bookmarks;
const bookmarksSelectedTagsKeyword = (state) => state.filters;

const getBookmarksByTagsAndText = (bookmarks, filterState) => {
    let tagFilter = filterState.tagFilter;
    let textFilter = filterState.searchText;

    return _.filter(
        bookmarks,
        bookmark => {
            let textResult = textFilter.length === 0; // ignore if text is empty
            let tagsResult = tagFilter.length === 0; // ignore if there is no tags selected
            if (textFilter.length > 0) {
                textFilter = textFilter.toLowerCase(); // ignore case
                textResult = _.includes(bookmark.title.toLowerCase(), textFilter) || _.includes(bookmark.url.toLowerCase(), textFilter);
            }
            if (tagFilter.length > 0) {
                tagsResult = _.includes(bookmark.tags, tagFilter)
            }
            return textResult && tagsResult;
        }
    );
};


export default createSelector(
    bookmarksSelector,
    bookmarksSelectedTagsKeyword,
    getBookmarksByTagsAndText // o último recebe os pedaços de estado criados acima
)