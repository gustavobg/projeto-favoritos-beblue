import { createSelector } from 'reselect'
import _ from 'lodash';

const tagsSelector = (state) => state.tags;

const getTagColorByIdSelector = (tags, name) => {
    return _.filter(
        tags,
        tag => {
            return tag.name === name;
        }
    );
};

export default createSelector(
    tagsSelector,
    getTagColorByIdSelector // o último recebe os pedaços de estado criados acima
)