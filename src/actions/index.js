let nextBookmarkId = 10;
export const addBookmark = (title, url, tags) => {
  return {
      type: 'ADD_BOOKMARK',
      id: (nextBookmarkId++).toString(),
      title,
      url,
      tags
  }
};
export const removeBookmark = (id) => {
    return {
        type: 'REMOVE_BOOKMARK',
        id
    }
};
export const editBookmark = (id, title, url, tags) => {
    return {
        type: 'EDIT_BOOKMARK',
        id,
        title,
        url,
        tags
    }
};
export const filterByTag = (tagName) => ({
    type: 'FILTER_BY_TAG',
    tagName
});

export const filterByText = (text) => ({
    type: 'FILTER_BY_TEXT',
    text
});


export const getTagById = (id) => {
    return {
        type: 'GET_TAG_BY_ID',
        id: id
    }
};

let colors = ['#F44336', '#E91E63', '#9C27B0', '#4A148C', '#880E4F', '#B71C1C', '#673AB7', '#3F51B5', '#2196F3', '#311B92', '#1A237E', '#0D47A1', '#03A9F4', '#00BCD4', '#009688', '#01579B', '#006064', '#004D40', '#4CAF50', '#8BC34A', '#CDDC39', '#1B5E20', '#33691E', '#827717', '#FFEB3B', '#FFC107', '#FFC107', '#FF5722', '#795548', '#9E9E9E', '#607D8B', '#000000'];
let c = 1;
export const addTag = (name, color) => {
    return {
        type: 'ADD_TAG',
        id: name,
        color: ((color) => {
            if (color)
                return color;
            if (colors.length === c)
                c = 0;

            let selectedColor = colors[c];
            c++;
            return selectedColor;
        })(color)
    }
};

