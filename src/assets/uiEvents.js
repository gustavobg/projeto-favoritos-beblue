let xDown = null;
let yDown = null;

const handleTouchStart = (evt) => {
    xDown = evt.touches[0].clientX;
    yDown = evt.touches[0].clientY;
};

const handleTouchMove = (evt) => {
    if ( ! xDown || ! yDown ) {
        return;
    }

    let xUp = evt.touches[0].clientX;
    let yUp = evt.touches[0].clientY;

    let xDiff = xDown - xUp;
    let yDiff = yDown - yUp;

    if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {/*most significant*/
        if ( xDiff > 0 ) {
            /* left swipe */
        } else {
            /* right swipe */
            if (document.body.classList.contains('search-is-open')) {
                document.body.classList.remove('search-is-open');
            }
        }
    } else {
        if ( yDiff > 0 ) {
            /* up swipe */
        } else {
            /* down swipe */
        }
    }
    /* reset values */
    xDown = null;
    yDown = null;
};

document.addEventListener('touchstart', handleTouchStart, false);
document.addEventListener('touchmove', handleTouchMove, false);

// trigger menu click
let specifiedElement = document.getElementById('search-aside');
let buttonToggleSearch = document.getElementById('button-toggle-search');

buttonToggleSearch.addEventListener('click', () => {
    if (document.body.classList.contains('search-is-open')) {
        document.body.classList.remove('search-is-open');
    } else {
        document.body.classList.add('search-is-open');
    }
});
document.addEventListener('click', (event) => {
    let isClickInside = specifiedElement.contains(event.target);
    isClickInside = event.target.getAttribute('id') && event.target.getAttribute('id') === 'button-toggle-search' ? true : isClickInside;

    if (!isClickInside) {
        // toggle menu off
        document.body.classList.remove('search-is-open');
    }
});

// has vertical scrollbar
const hasScroll = () => {
    let hasVerticalScrollbar = document.body.scrollTop > 0;
    if (hasVerticalScrollbar) {
        document.body.classList.add('has-scrollbar-v');
    } else {
        document.body.classList.remove('has-scrollbar-v');
    }
};
window.addEventListener('resize', hasScroll);
window.addEventListener('scroll', hasScroll);