# README #

App de favoritos para processo seletivo Beblüe

### Instalação ###

* Instalar o Node ([https://nodejs.org/en/](https://nodejs.org/en/))
* Instalar pacotes e dependências (npm install)

### Como utilizar ###

Iniciar desenvolvimento 
```
#!python

npm run start
```

Publicar para produção 
```
#!python

npm run build:dist
```

Publicar arquivos para iniciar desenvolvimento 
```
#!python

npm run build:dev
```

Realizar testes unitários 
```
#!python

npm run test
```


### Por: Gustavo Gebrim ###